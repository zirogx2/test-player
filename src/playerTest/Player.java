
package playerTest;
import java.util.ArrayList; 
public class Player {
	int health; //if 0 remain player dies, can be used as currency to perform physical moves
	int mana; //currency used to perform magic based moves
	int strength; //will affect attack power
	int dexterity; //affects hit chance
	int luck; //affects critical hits and xp gain
	int experience; //progression tool (collect 100 to level up)
	int charisma; //affects barter chance and allows certain events
	int toughness; //affects damage intake
	
	
	//Items class not yet implimented (Inventory will hold Item objects that have ids and quantity that can change after use ) equipment will hold item objects that are equipped to the player
	//ArrayList<Items> inventory = new ArrayList<Items>;
	//ArrayList<Items> equipment = new ArrayList<Items>;
	
	
	
	public int levelCheck() { //100 xp between levels
		return this.experience / 100; 
	}
	public int getCharisma() {
		return charisma;
	}
	public void setCharisma(int charisma) {
		this.charisma = charisma;
	}
	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}
	public int getMana() {
		return mana;
	}
	public void setMana(int mana) {
		this.mana = mana;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public int getDexterity() {
		return dexterity;
	}
	public void setDexterity(int dexterity) {
		this.dexterity = dexterity;
	}
	public int getLuck() {
		return luck;
	}
	public void setLuck(int luck) {
		this.luck = luck;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	public int getToughness() {
		return toughness;
	}
	public void setToughness(int toughness) {
		this.toughness = toughness;
	}
	

}
